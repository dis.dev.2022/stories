"use strict"

import Swiper, {Autoplay, Navigation, Pagination, EffectCreative} from 'swiper';

const optionsJSON = {
    "options": {
        "copyright": "enable",
        "custom_html": "Hello, <b>world</b>",
        "history": true,
        "outline_color": "#C6C6C6"
    },

    "groups": [
        {
            "preview": "img/preview__01.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__03.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026723411ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация Регистрация Регистрация Регистрация Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__02.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация Регистрация Регистрация "
                },
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__02.mp4",
                    "cta_button": "disabled"
                },
            ]
        },
        {
            "preview": "img/preview__03.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__04.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__05.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
            ]
        },
        {
            "preview": "img/preview__01.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026723411ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__02.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__03.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__04.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__05.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
    ]
}

function mediaWidget (data, selector = '[data-stories]') {

    document.querySelector(selector).attachShadow({mode: 'open'})

    const container = document.querySelector(selector).shadowRoot;
    let previewSlides = ''
    let groupSlider = null;
    let shortSlider = null;
    let shortSliderPagination = null;

    // Первичная разметка
    function generateHTML() {
        // Base structure
        container.innerHTML =
            '<div class="vw_stories">' +
            '<link rel="stylesheet" href="css/main.min.css">' +
            '<div class="vw_preview">' +
            '</div>' +
            '<div class="vw_shorts">' +
            '</div>' +
            '</div>'

        // Preview source
        data.groups.forEach(function (elem, index) {
            previewSlides =  previewSlides +
                '<div class="swiper-slide">' +
                '<div class="vw_media-preview__item" data-preview-item="' + index + '">' +
                '<div class="vw_media-preview__image">' +
                '<img src="' + elem.preview + '" class="img-cover" alt="">' +
                '</div>' +
                '</div>' +
                '</div>' + '\n'
        });

        // Add Preview to html
        container.querySelector('.vw_preview').innerHTML =
            '<div class="vw_media-preview">' +
            '<div class="swiper" data-preview>' +
            '<div class="swiper-wrapper" data-preview>' +
            previewSlides +
            '</div>' +
            '</div>' +
            '<button type="button" class="vw_media-preview__nav vw_media-preview__nav--next" data-preview-next><i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.243 13.501" class="ico-svg"><path d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z" transform="translate(-7.875 -11.252)"/></svg></i></button>' +
            '<button type="button" class="vw_media-preview__nav vw_media-preview__nav--prev" data-preview-prev><i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.243 13.501" class="ico-svg"><path d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z" transform="translate(28.118 24.753) rotate(180)"/></svg></i></button>' +
            '</div>'

        container.querySelector('.vw_shorts').innerHTML =
            '<div class="vw_media-short" data-media-content>\n' +
            '    <div class="vw_media-short__container">\n' +
            '        <div class="vw_media-short__groups">\n' +
            '            <div class="swiper" data-media-groups>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '<div class="vw_media-short__overlay vw_media-short__overlay--prev" data-media-prev></div>\n' +
            '<div class="vw_media-short__overlay vw_media-short__overlay--next" data-media-next></div>\n' +
            '        <div class="vw_media-short__navigation">\n' +
            '            <button type="button" class="vw_media-short__nav vw_media-short__nav--prev" data-media-prev>\n' +
            '                <i>\n' +
            '                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.243 13.501" class="img-fluid">\n' +
            '                        <path d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z" transform="translate(28.118 24.753) rotate(180)"/>\n' +
            '                    </svg>\n' +
            '                </i>\n' +
            '            </button>\n' +
            '            <button type="button" class="vw_media-short__nav vw_media-short__nav--next" data-media-next>\n' +
            '                <i>\n' +
            '                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.243 13.501" class="img-fluid">\n' +
            '                        <path d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z" transform="translate(-7.875 -11.252)"/>\n' +
            '                    </svg>\n' +
            '                </i>\n' +
            '            </button>\n' +
            '        </div>\n' +
            '        <button type="button" class="vw_media-short__close" data-media-close><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 64.983" class="ico-svg"><path d="M51.487,43.78,74.7,20.565a5.44,5.44,0,0,0-7.693-7.693L43.794,36.087,20.578,12.872a5.44,5.44,0,1,0-7.693,7.693L36.1,43.78,12.885,67a5.44,5.44,0,0,0,7.693,7.693L43.794,51.473,67.009,74.689A5.44,5.44,0,0,0,74.7,67Z" transform="translate(-11.285 -11.289)" fill="#f9f9f9" opacity="0.9"></path></svg></button>\n' +
            '        <div class="vw_media-short__control">\n' +
            '            <button class="vw_short-sound" data-sound>\n' +
            '                <i class="vw_short-sound__off">\n' +
            '                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65.595 54.665" class="img-fluid">\n' +
            '                        <path d="M24.488,7.394,14.356,17.524H2.733A2.732,2.732,0,0,0,0,20.257v16.4a2.733,2.733,0,0,0,2.733,2.733H14.356l10.132,10.13a2.735,2.735,0,0,0,4.666-1.933V9.327A2.735,2.735,0,0,0,24.488,7.394Zm26.57-5.817a2.756,2.756,0,0,0-3.025,4.608,26.608,26.608,0,0,1,0,44.545,2.755,2.755,0,1,0,3.025,4.606,32.12,32.12,0,0,0,0-53.759Zm3.6,26.879a21.026,21.026,0,0,0-9.767-17.793,2.721,2.721,0,0,0-3.772.85,2.766,2.766,0,0,0,.844,3.8,15.564,15.564,0,0,1,0,26.288,2.765,2.765,0,0,0-.844,3.8,2.727,2.727,0,0,0,3.772.85A21.025,21.025,0,0,0,54.662,28.456ZM38.518,19.7a2.734,2.734,0,0,0-2.637,4.79,4.489,4.489,0,0,1,0,7.928,2.734,2.734,0,1,0,2.637,4.79,9.957,9.957,0,0,0,0-17.508Z" transform="translate(0 -1.125)" fill="#f9f9f9" opacity="0.9"/>\n' +
            '                    </svg>\n' +
            '                </i>\n' +
            '                <i class="vw_short-sound__on">\n' +
            '                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96.468 72.35" class="img-fluid">\n' +
            '                        <path d="M40.514,5.828,23.751,22.587H4.522A4.521,4.521,0,0,0,0,27.109V54.24a4.522,4.522,0,0,0,4.522,4.522H23.751L40.514,75.522a4.525,4.525,0,0,0,7.719-3.2V9.025A4.525,4.525,0,0,0,40.514,5.828ZM86.979,40.675l8.6-8.6a3.041,3.041,0,0,0,0-4.3l-4.3-4.3a3.041,3.041,0,0,0-4.3,0l-8.6,8.6-8.6-8.6a3.041,3.041,0,0,0-4.3,0l-4.3,4.3a3.041,3.041,0,0,0,0,4.3l8.6,8.6-8.6,8.6a3.041,3.041,0,0,0,0,4.3l4.3,4.3a3.041,3.041,0,0,0,4.3,0l8.6-8.6,8.6,8.6a3.041,3.041,0,0,0,4.3,0l4.3-4.3a3.041,3.041,0,0,0,0-4.3Z" transform="translate(0 -4.5)" fill="#f9f9f9" opacity="0.9"/>\n' +
            '                    </svg>\n' +
            '                </i>\n' +
            '            </button>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>'
    }

    generateHTML()

    const shortWidget = container.querySelector('.vw_stories')
    const shortBorderColor = data.options.outline_color;
    const mediaCopyrightText = data.options.custom_html;
    const mediaCopyrightOption = data.options.copyright;
    const mediaCopyright = container.querySelector('[data-media-copyright]')

    const previewSliderPrev = container.querySelector('[data-preview-prev]')
    const previewSliderNext = container.querySelector('[data-preview-next]')

    // Слайдер превьюшек
    const previewSlider = new Swiper(container.querySelector('[data-preview]'), {
        modules: [Navigation],
        observer: true,
        observeParents: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        autoHeight: true,
        speed: 400,
        breakpoints: {
            1200: {
                spaceBetween: 10,
            }
        },
        navigation: {
            nextEl: previewSliderNext,
            prevEl: previewSliderPrev,
        },
    });

    // Содержимое шортов
    function shortSliderContent(idx = 0) {
        let media = ''
        let shortSlides = ''
        let staButton = ''

        data.groups[idx].stories.forEach(function (elem, index){
            if (elem.media_type === 'video') {
                media = '<video data-short-video muted src="' + elem.media_src + '" playsinline webkit-playsinline preload="auto"></video>'
            }
            else {
                media = '<img src="'+ elem.media_src +'" class="img-cover" alt="">'
            }

            if (elem.cta_button === 'disabled') {
                staButton = ''
            }
            else {
                staButton = '<div class="vw_short-item__button">' +
                    '<a href="' + elem.cta_button_href + '" class="vw_short-button" data-short-button style="background-color: '+ elem.cta_button_color_bg +'; color: '+ elem.cta_button_color_text +';">' +
                    '<span class="vw_short-button__text" data-fontfit>' + elem.cta_button_text + '</span>' +
                    '</a>' +
                    '</div>'
            }

            shortSlides = shortSlides +
                '<div class="swiper-slide">' +
                '<div class="vw_short-item">' +
                '<div class="vw_short-item__media">' +
                media +
                '</div>' +
                staButton +
                '</div>' +
                '</div>' + '\n\n'
        })
        return shortSlides
    }

    // Слайдер Шортов
    function groupSliderContent() {
        let groupSlides = ''
        data.groups.forEach(function (elem, index) {
            groupSlides =  groupSlides +
                '<div class="swiper-slide">\n\n' +

                '<div class="swiper" data-short>\n' +
                '<div class="swiper-wrapper">\n' +
                shortSliderContent(index) +
                '</div>\n' +
                '<div class="vw_short-pagination" data-short-pagination></div>' +
                '</div>' +
                '        <div class="vw_media-short__copyright" data-media-copyright>\n' +
                '            <a href="#" class="vw_media-copyright">\n' +
                '                <span class="vw_media-copyright__text" data-media-copyright-text>Сделано с помощью</span>\n' +
                '                <i class="vw_media-copyright__logo">\n' +
                '                    <img src="img/logo.svg" class="ico-svg" alt="">\n' +
                '                </i>\n' +
                '            </a>\n' +
                '        </div>\n' +
                '\n\n</div>' + '\n\n\n'
        });
        groupSlides =
            '<div class="swiper-wrapper">\n\n' +
            groupSlides +
            '</div>'
        return groupSlides
    }
    container.querySelector('[data-media-groups]').innerHTML = groupSliderContent()

    const videos = container.querySelector('[data-media-content]').querySelectorAll('video')

    // Инициализация слайдера групп шортов
    function groupSliderInit(slide = 0) {
        if (!groupSlider) {
            groupSlider = new Swiper(container.querySelector('[data-media-groups]'), {
                modules: [Navigation],
                observer: true,
                initialSlide: slide,
                observeParents: true,
                allowTouchMove: true,
                slidesPerView: 1,
                spaceBetween: 32,
                autoHeight: false,
                speed: 400,
                on: {
                    afterInit: function (event) {
                        const activeSlide = event.el.querySelector('.swiper-slide-active').querySelector('[data-short]')
                        const videos = event.el.querySelectorAll('video')
                        if ( videos.length > 0) {
                            event.el.querySelectorAll('video').forEach(elem => {
                                let videoDuration = parseInt(elem.duration)
                                let videoAutoplay = videoDuration + 's'
                                elem.closest('.swiper-slide').dataset.swiperAutoplay = (videoDuration * 1000)
                                shortWidget.style.setProperty('--short-video-duration', videoAutoplay);
                            })
                        }

                        shortSliderInit(activeSlide)
                        previewSlider.slides[this.realIndex].querySelector('[data-preview-item]').classList.add('-passed-')

                        container.addEventListener('click', (event) => {
                            if (event.target.closest('.swiper-slide-prev')) {
                                console.log('groupSliderPrev')
                                groupSlider.slidePrev(400)
                            }
                        })
                        container.addEventListener('click', (event) => {
                            if (event.target.closest('.swiper-slide-next')) {
                                console.log('groupSliderNext')
                                groupSlider.slideNext(400)
                            }
                        })
                    },
                    slideChange: function () {
                        previewSlider.slides[this.realIndex].querySelector('[data-preview-item]').classList.add('-passed-')
                    },
                    slideChangeTransitionStart: function (el) {
                        shortSliderDestroy()
                    },
                    slideChangeTransitionEnd: function (el) {
                        const activeSlide = el.slidesEl.querySelector('.swiper-slide-active').querySelector('[data-short]')
                        shortSliderInit(activeSlide)
                    }
                }
            });
        }
    }

    // Удаление слайдера слайдера групп шортов
    function groupSliderDestroy() {
        if(groupSlider) {
            groupSlider.destroy();
            groupSlider = null;
            container.querySelector('[data-media-content]').classList.remove('-open-');
        }
    }

    // Инициализация активного слайдера шортов
    function shortSliderInit(selector) {
        if(!shortSlider) {
            shortSliderPagination = selector.querySelector('[data-short-pagination]')
            shortSlider = new Swiper(selector, {
                modules: [Pagination, Autoplay, EffectCreative],
                observer: true,
                observeParents: true,
                nested: true,
                slidesPerView: 1,
                spaceBetween: 0,
                speed: 400,
                autoplay: {
                    delay: 10000,
                    disableOnInteraction: false
                },
                grabCursor: true,
                effect: "creative",
                creativeEffect: {
                    prev: {
                        shadow: true,
                        translate: [0, 0, -400],
                    },
                    next: {
                        translate: ["100%", 0, 0],
                    },
                },
                pagination: {
                    el: shortSliderPagination,
                    clickable: true,
                    bulletClass: 'vw_short-pagination__bullet',
                    bulletActiveClass: 'vw_short-pagination__bullet--active',
                    renderBullet: function (index, className) {
                        return '<div class="' + className + '"><div class="vw_short-pagination__progress"></div></div>';
                    },
                },
                on: {
                    beforeInit: function (event) {
                    },
                    afterInit: function (event) {

                        if (event.slides[0].querySelector('video')) {
                            let aDuration = event.slides[0].dataset.swiperAutoplay + 'ms'
                            event.el.querySelector('.vw_short-pagination__bullet--active').querySelector('.vw_short-pagination__progress').style.animationDuration = aDuration
                            event.slides[0].querySelector('video').play()
                            container.querySelector('[data-media-content]').classList.add('-enable-video-control-')
                        }
                    },
                    slideChangeTransitionStart: function (event) {
                        // Запуск видеоролика с начальной позиции при переключении на слайд с видео
                        const slide =  event.slidesEl.querySelector('.swiper-slide-active')
                        const video = slide.querySelector('[data-short-video]')
                        if(video) {
                            video.currentTime = 0
                            let videoDuration = parseInt(video.duration) + 's'
                            event.el.querySelector('.vw_short-pagination__bullet--active').querySelector('.vw_short-pagination__progress').style.animationDuration = videoDuration
                            container.querySelector('[data-media-content]').classList.add('-enable-video-control-')
                            video.play();
                        }
                    },
                    beforeSlideChangeStart: function (event) {
                //        event.pagination.el.querySelector('.vw_short-pagination__bullet--active').classList.add('-passed-')
                    },
                    slideChange: function (event) {
                        event.pagination.bullets.forEach(elem => {
                            elem.classList.remove('-passed-');
                        })
                        for (let index = 0; index < event.realIndex; ++index) {
                            const element = event.pagination.bullets[index];
                            element.classList.add('-passed-')
                        }

                        const slide =  event.slidesEl.querySelector('.swiper-slide-active')
                        const video = slide.querySelector('[data-short-video]')

                        if(video) {
                            video.pause();
                            container.querySelector('[data-media-content]').classList.remove('-enable-video-control-')
                        }
                    },
                    beforeDestroy: function (event) {
                        if (container.querySelector('.short-pagination__bullet--active')) {
                            container.querySelector('.short-pagination__bullet--active').classList.remove('-bullet-video-')
                            shortWidget.style.setProperty('--short-video-duration', '10s');
                        }
                    },
                    autoplay: function (event) {
                        if(event.realIndex === 0) {
                            let num = groupSlider.slides.length - groupSlider.activeIndex
                            if (num === 1 ) {
                                event.autoplay.stop()
                                shortSliderDestroy()
                                groupSliderDestroy()
                                container.querySelector('[data-media-content]').classList.remove('-open-');
                            }
                            else {
                                event.autoplay.stop()
                                groupSlider.slideNext(400)
                            }
                        }
                    }
                }
            });
        }
    }

// Удаление экземпляра слайдера шортов
    function shortSliderDestroy() {
        if(shortSlider) {
            shortSlider.destroy();
            shortSlider = null;
            shortSliderPagination = null;
            stopVideos()
        }
    }

    // Остановка всех видео + скрытие кнопки звука.
    function stopVideos() {
        videos.forEach(elem => {
            elem.pause();
            elem.currentTime = 0
            container.querySelector('[data-media-content]').classList.remove('-enable-video-control-')
        });
    }

    // Open Media
    container.addEventListener('click', (event) => {
        if (event.target.closest('[data-preview-item]')) {
            const shortGroup = parseInt(event.target.closest('[data-preview-item]').dataset.previewItem)
            container.querySelector('[data-media-content]').classList.add('-open-');
            groupSliderInit(shortGroup)
        }
    })

    // Close Media
    container.addEventListener('click', (event) => {
        if (event.target.closest('[data-media-close]')) {
            container.querySelector('[data-media-content]').classList.remove('-open-');
            shortSliderDestroy()
            groupSliderDestroy()
            stopVideos()
        }
    })

    // Sound on|off
    container.addEventListener('click', (event) => {
        if (event.target.closest('[data-sound]')) {
            console.log('data-sound')
            container.querySelector('[data-sound]').classList.toggle('-muted-');
            videos.forEach(elem => {
                if (elem.muted === false) {
                    elem.muted = true;
                }
                else {
                    elem.muted = false;
                }
            });
        }
    })

    // Short Slider Slider Nav
    container.addEventListener('click', (event) => {
        if (event.target.closest('[data-media-prev]')) {
            shortSlider.slidePrev(400)
        }
        else if (event.target.closest('[data-media-next]')) {
            shortSlider.slideNext(400)
        }
    })

    document.addEventListener('keydown', function(event) {
        const key = event.key;
        let slideNum = 0
        if (groupSlider) {
            switch (event.key) {
                case 'ArrowLeft':
                    slideNum = shortSlider.realIndex
                    shortSlider.slidePrev(400)
                    if (slideNum === shortSlider.realIndex) {
                        groupSlider.slidePrev(400)
                    }
                    break;
                case 'ArrowRight':
                    slideNum = shortSlider.realIndex
                    shortSlider.slideNext(400)
                    if (slideNum === shortSlider.realIndex) {
                        groupSlider.slideNext(400)
                    }
                    break;
                case 'Escape':
                    groupSliderDestroy()
                    break;
            }
        }
        else {
            switch (event.key) {
                case 'ArrowLeft':
                    previewSlider.slidePrev(400)
                    break;
                case 'ArrowRight':
                    previewSlider.slideNext(400)
                    break;
            }
        }
    })

    container.querySelector('[data-preview]').addEventListener('wheel', (event) => {
        console.log(event.deltaY);
        if(event.deltaY > 0) {
            previewSlider.slideNext(400)
        }
        else {
            previewSlider.slidePrev(400)
        }
    })

    if (shortBorderColor) {
        shortWidget.style.setProperty('--short-border-color', shortBorderColor);
    }

    if (mediaCopyrightOption === 'disable') {
        mediaCopyright.remove()
        shortWidget.style.setProperty('--short-button-padding', '30px');
    }

    if (mediaCopyrightOption === 'custom') {
        container.querySelectorAll('[data-media-copyright-text]').forEach(elem => {
            elem.classList.innerHTML = mediaCopyrightText
        });
    }
}


mediaWidget(optionsJSON)

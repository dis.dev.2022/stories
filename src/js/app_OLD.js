"use strict"

import Swiper, {Autoplay, Navigation, Pagination, EffectCreative} from 'swiper';

const optionsJSON = {
    "options": {
        "copyright": "enable",
        "custom_html": "Hello, <b>world</b>",
        "history": true,
        "outline_color": "#C6C6C6"
    },

    "groups": [
        {
            "preview": "img/preview__01.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__03.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026723411ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация Регистрация Регистрация Регистрация Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__02.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация Регистрация Регистрация "
                },
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__02.mp4",
                    "cta_button": "disabled"
                },
            ]
        },
        {
            "preview": "img/preview__03.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__04.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__05.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
            ]
        },
        {
            "preview": "img/preview__01.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026723411ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-444d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__02.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__03.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__04.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
        {
            "preview": "img/preview__05.jpg",
            "stories": [
                {
                    "uuid": "6bdd6387-5d4e-4354d-82ad-c026792811ed",
                    "media_type": "image",
                    "media_src": "img/st__01.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-480d-82ad-c026792822ed",
                    "media_type": "image",
                    "media_src": "img/st__02.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-440d-82ad-c026792833ed",
                    "media_type": "image",
                    "media_src": "img/st__03.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Регистрация"
                },
                {
                    "uuid": "6bdd6387-5d4e-414d-82ad-c026792811e4",
                    "media_type": "video",
                    "media_src": "img/video__01.mp4",
                    "cta_button": "disabled"
                },
                {
                    "uuid": "6bdd6387-5d4e-442d-82ad-c034792833ed",
                    "media_type": "image",
                    "media_src": "img/st__04.jpg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Получить предложение"
                },
                {
                    "uuid": "6bdd6387-5d4e-443d-82ad-c085792833ed",
                    "media_type": "image",
                    "media_src": "img/st__05.jpeg",
                    "cta_button": "href",
                    "cta_button_href": "#",
                    "cta_button_color_bg": "#ffb13d",
                    "cta_button_color_text": "#000000",
                    "cta_button_text": "Заказать"
                },
            ]
        },
    ]
}


function mediaWidget (options) {

    const shortBorderColor = options.options.outline_color;
    const mediaCopyrightText = options.options.custom_html;
    const mediaCopyrightOption = options.options.copyright;
    const mediaCopyright = document.querySelector('[data-media-copyright]')

    const shortWidget = document.querySelector('[data-media-widget]')
    const previewSlider = document.querySelector('[data-preview]')
    const videos = document.querySelector('[data-media-content]').querySelectorAll('video')

    let previewSlides = ''
    let groupSlider = null;
    let shortSlider = null;

    // Содержимое превьюшек
    options.groups.forEach(function (elem, index) {
        previewSlides =  previewSlides +
            '<div class="swiper-slide">' +
            '<div class="vw_media-preview__item" data-preview-item="' + index + '">' +
            '<div class="vw_media-preview__image">' +
            '<img src="' + elem.preview + '" class="img-cover" alt="">' +
            '</div>' +
            '</div>' +
            '</div>' + '\n'
    });

    previewSlider.querySelector('.swiper-wrapper').innerHTML = previewSlides

    function shortSliderContent(idx = 0) {

        let media = ''
        let shortSlides = ''
        let staButton = ''
        options.groups[idx].stories.forEach(function (elem, index){
            if (elem.media_type === 'video') {
                media = '<video data-short-video muted src="' + elem.media_src + '" playsinline webkit-playsinline preload="auto"></video>'
            }
            else {
                media = '<img src="'+ elem.media_src +'" class="img-cover" alt="">'
            }

            if (elem.cta_button === 'disabled') {
                staButton = ''
            }
            else {
                staButton = '<div class="vw_short-item__button">' +
                    '<a href="' + elem.cta_button_href + '" class="vw_short-button" data-short-button style="background-color: '+ elem.cta_button_color_bg +'; color: '+ elem.cta_button_color_text +';">' +
                    '<span class="vw_short-button__text" data-fontfit>' + elem.cta_button_text + '</span>' +
                    '</a>' +
                    '</div>'
            }

            shortSlides = shortSlides +
                '<div class="swiper-slide">' +
                '<div class="vw_short-item">' +
                '<div class="vw_short-item__media">' +
                media +
                '</div>' +
                staButton +
                '</div>' +
                '</div>' + '\n\n'
        })
        return shortSlides
    }

    function groupSliderContent() {
        let groupSlides = ''
        options.groups.forEach(function (elem, index) {
            groupSlides =  groupSlides +
                '<div class="swiper-slide">\n\n' +
                '<div class="swiper" data-short>\n' +
                '<div class="swiper-wrapper">\n' +
                shortSliderContent(index) +
                '</div>\n' +
                '<div class="vw_short-pagination" data-short-pagination></div>' +
                '</div>' +
                '\n\n</div>' + '\n\n\n'
        });
       return groupSlides
    }

    document.querySelector('[data-media-groups]').querySelector('.swiper-wrapper').innerHTML = groupSliderContent()


  //  console.log();

    // Слайдер превьюшек
    new Swiper('[data-preview]', {
        modules: [Navigation],
        observer: true,
        observeParents: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        autoHeight: true,
        speed: 400,
        breakpoints: {
            1200: {
                spaceBetween: 20,
            }
        },
        navigation: {
            nextEl: '[data-preview-next]',
            prevEl: '[data-preview-prev]',
        },
    });

// Инициализация слайдера групп шортов
    function groupSliderInit(slide = 0) {
        if (!groupSlider) {
            groupSlider = new Swiper('[data-media-groups]', {
                modules: [Navigation],
                observer: true,
                initialSlide: slide,
                observeParents: true,
                allowTouchMove: false,
                slidesPerView: 1,
                spaceBetween: 32,
                autoHeight: false,
                speed: 400,
                navigation: {
                    nextEl: '[data-media-next]',
                    prevEl: '[data-media-prev]',
                },
                on: {
                    afterInit: function (event) {
                        const activeSlide = event.el.querySelector('.swiper-slide-active').querySelector('[data-short]')
                        const videos = event.el.querySelectorAll('video')
                        if ( videos.length > 0) {
                            event.el.querySelectorAll('video').forEach(elem => {
                                let videoDuration = parseInt(elem.duration)
                                let videoAutoplay = videoDuration + 's'
                                elem.closest('.swiper-slide').dataset.swiperAutoplay = (videoDuration * 1000)
                                shortWidget.style.setProperty('--short-video-duration', videoAutoplay);
                            })
                        }

                        shortSliderInit(activeSlide)
                    },
                    slideChangeTransitionStart: function (el) {
                        shortSliderDestroy()
                    },
                    slideChangeTransitionEnd: function (el) {
                        const activeSlide = el.slidesEl.querySelector('.swiper-slide-active').querySelector('[data-short]')
                        shortSliderInit(activeSlide)
                    }
                }
            });
        }
    }

// Удаление слайдера слайдера групп шортов
    function groupSliderDestroy() {
        if(groupSlider) {
            groupSlider.destroy();
            groupSlider = null;
            document.querySelector('[data-media-content]').classList.remove('-open-');
        }
    }

// Инициализация активного слайдера шортов
    function shortSliderInit(selector) {
        if(!shortSlider) {
            shortSlider = new Swiper(selector, {
                modules: [Pagination, Autoplay, Navigation, EffectCreative],
                observer: true,
                observeParents: true,
                nested: true,
                slidesPerView: 1,
                spaceBetween: 0,
                speed: 400,
                autoplay: {
                    delay: 10000,
                    disableOnInteraction: false
                },
                grabCursor: true,
                effect: "creative",
                creativeEffect: {
                    prev: {
                        shadow: true,
                        translate: [0, 0, -400],
                    },
                    next: {
                        translate: ["100%", 0, 0],
                    },
                },
                navigation: {
                    nextEl: '[data-short-next]',
                    prevEl: '[data-short-prev]',
                },
                pagination: {
                    el: '[data-short-pagination]',
                    clickable: true,
                    bulletClass: 'vw_short-pagination__bullet',
                    bulletActiveClass: 'vw_short-pagination__bullet--active',
                    renderBullet: function (index, className) {
                        return '<div class="' + className + '"><div class="vw_short-pagination__progress"></div></div>';
                    },
                },
                on: {
                    beforeInit: function (event) {
                        // Умеем есть HTMLCollection элементов
                        function fontfit (collection) {
                            var item, textWidth, width, lineHeight, fontSize,
                                saveNativeStyle = function(item, rules) {
                                    var style = getComputedStyle(item);
                                    item.fontfitsave = item.fontfitsave || {};
                                    for(var i = 0; i < rules.length; i++) {
                                        item.fontfitsave[rules[i]] = style[rules[i]];
                                    }
                                },
                                restoreNativeStyle = function(item, rules) {
                                    for(var i = 0; i < rules.length; i++) {
                                        item.style[rules[i]] = item.fontfitsave[rules[i]];
                                    }
                                };

                            // Проходимся по каждому в коллекции
                            for(var i = 0; i < collection.length; i++) {
                                item = collection[i];

                                // сохраняем текущие стили чтоб восстановить позже
                                saveNativeStyle(item, ["display"]);

                                // задаём инлайновое отображение в одну строчку чтоб узнать длину текста
                                item.style.whiteSpace = "nowrap";
                                item.style.display = "inline-block";
                                textWidth = item.clientWidth;

                                // теперь делаемся блоковым, чтоб понять на какую ширину мы рассчитываем
                                item.style.display = "block";
                                width = item.clientWidth;

                                // Восстанавливаем исходные стили
                                restoreNativeStyle(item, ["display"]);

                                // вычисляем и ставим новый font-size
                                fontSize = Math.floor((width / textWidth) * parseInt(getComputedStyle(item).fontSize));
                                item.style.fontSize = fontSize + "px";

                                // - а также подходящий line-height
                                let appFontSize = parseInt(getComputedStyle(document.body).fontSize);
                                let appLineHeight = parseInt(getComputedStyle(document.body).lineHeight);
                                item.style.lineHeight = (fontSize * appLineHeight / appFontSize) + "px";

                                // send event
                                item.dispatchEvent(new CustomEvent('fontfit', { bubbles: true }));
                            }
                        };

                        function processFontfit () {
                            requestAnimationFrame(function () {
                                fontfit(document.querySelectorAll('[data-fontfit]'));
                            });
                        }

                        // Do fontfit on various events
                        document.addEventListener('DOMContentLoaded', processFontfit);
                        window.addEventListener('load', processFontfit);
                        window.addEventListener('resize', processFontfit);

                        // Add global function
                        if (!window.fontfit) {
                            window.fontfit = fontfit;
                        }
                    },
                    afterInit: function (event) {

                        if (event.slides[0].querySelector('video')) {
                            let aDuration = event.slides[0].dataset.swiperAutoplay + 'ms'
                            event.el.querySelector('.vw_short-pagination__bullet--active').querySelector('.vw_short-pagination__progress').style.animationDuration = aDuration
                            event.slides[0].querySelector('video').play()
                            document.querySelector('[data-media-content]').classList.add('-enable-video-control-')
                        }
                    },
                    slideChangeTransitionStart: function (event) {
                        // Запуск видеоролика с начальной позиции при переключении на слайд с видео
                        const slide =  event.slidesEl.querySelector('.swiper-slide-active')
                        const video = slide.querySelector('[data-short-video]')
                        if(video) {
                            video.currentTime = 0
                            let videoDuration = parseInt(video.duration) + 's'
                            event.el.querySelector('.vw_short-pagination__bullet--active').querySelector('.vw_short-pagination__progress').style.animationDuration = videoDuration
                            document.querySelector('[data-media-content]').classList.add('-enable-video-control-')
                            video.play();
                        }
                    },
                    slideChange: function (event) {
                        const slide =  event.slidesEl.querySelector('.swiper-slide-active')
                        const video = slide.querySelector('[data-short-video]')

                        if(video) {
                            video.pause();
                            document.querySelector('[data-media-content]').classList.remove('-enable-video-control-')
                        }
                    },
                    beforeDestroy: function (event) {
                        if (document.querySelector('.short-pagination__bullet--active')) {
                            document.querySelector('.short-pagination__bullet--active').classList.remove('-bullet-video-')
                            shortWidget.style.setProperty('--short-video-duration', '10s');
                        }
                    },
                    autoplay: function (event) {
                        if(event.realIndex === 0) {
                            let num = groupSlider.slides.length - groupSlider.activeIndex
                            if (num === 1 ) {
                                event.autoplay.stop()
                                shortSliderDestroy()
                                groupSliderDestroy()
                                document.querySelector('[data-media-content]').classList.remove('-open-');
                            }
                            else {
                                event.autoplay.stop()
                                groupSlider.slideNext(400)
                            }
                        }
                    }
                }
            });
        }
    }

// Удаление экземпляра слайдера шортов
    function shortSliderDestroy() {
        if(shortSlider) {
            shortSlider.destroy();
            shortSlider = null;
            stopVideos()
        }
    }

// Остановка всех видео + скрытие кнопки звука.
    function stopVideos() {
        videos.forEach(elem => {
            elem.pause();
            document.querySelector('[data-media-content]').classList.remove('-enable-video-control-')
        });
    }


// Open Media
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-preview-item]')) {
            const shortGroup = parseInt(event.target.closest('[data-preview-item]').dataset.previewItem)
            document.querySelector('[data-media-content]').classList.add('-open-');
            groupSliderInit(shortGroup)
        }
    })

// Close Media
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-media-close]')) {
            document.querySelector('[data-media-content]').classList.remove('-open-');
            shortSliderDestroy()
            groupSliderDestroy()
            stopVideos()
        }
    })

// Sound on|off
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-sound]')) {
            document.querySelector('[data-sound]').classList.toggle('-muted-');
            videos.forEach(elem => {
                if (elem.muted === false) {
                    elem.muted = true;
                }
                else {
                    elem.muted = false;
                }
            });
        }
    })

    if (shortBorderColor) {
        shortWidget.style.setProperty('--short-border-color', shortBorderColor);
    }

    if (mediaCopyrightOption === 'disable') {
        mediaCopyright.remove()
        shortWidget.style.setProperty('--short-button-padding', '30px');
    }

    if (mediaCopyrightOption === 'custom') {
        document.querySelector('[data-media-copyright-text]').innerHTML = mediaCopyrightText
    }
}



mediaWidget(optionsJSON)
